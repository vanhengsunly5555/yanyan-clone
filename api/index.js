
// const apiUrl = 'http://47.57.10.85:10191/api' //测试
const apiUrl = 'http://einvest-v1.admin.hermitcrab168.top/api' // Testing server
const req = (url, data, isloading = false) => {
	return new Promise((resolve, reject) => {
		isloading && uni.showLoading({
			mask: true
		})
		
		var lang = uni.getStorageSync('language')
		if (lang == '') {
			lang = 'en-us'
		}
		
		if (uni.getStorageSync('token')) {
			data = {
				...data,
				token: uni.getStorageSync('token')
			}
		}
		uni.request({
			url: apiUrl + url + "?lang=" + lang,
			data: data,
			header: {
				// "Content-Type": "application/json",
				"Content-Type": "application/x-www-form-urlencoded"
			},
			method: 'POST',
			success: function(res) {
				resolve(res.data)
			},
			fail: function(e) {
				reject(e)
			},
			complete(res) {
				// console.log(res)
				isloading && uni.hideLoading()
				if (res.data.code == 0) {
					if(res.data.msg != ''){
						uni.showToast({
							title: res.data.msg,
							icon: "none",
							duration: 2000,
						})
					}
				}
				if (res.data.code == 401) {
					// uni.clearStorageSync()
					uni.removeStorageSync('token');
					uni.removeStorageSync('userinfo');
					uni.reLaunch({
						url:"/pages/login/index"
					})
				}
			}
		})
	})
}
export {
	req,
	apiUrl
}

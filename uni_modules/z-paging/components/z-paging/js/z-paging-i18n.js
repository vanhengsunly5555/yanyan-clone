// z-paging国际化(支持中文、中文繁体和英文)

const i18nUpdateKey = 'z-paging-i18n-update';

const refresherDefaultText = {
	'en-us': 'Pull down to refresh',
	'zh-cn': '继续下拉刷新',
	'jp': 'プルダウンを続けて更新します',
	'kr': '새로고침하려면 계속 아래로 당깁니다.',
}
const refresherPullingText = {
	'en-us': 'Release to refresh',
	'zh-cn': '松开立即刷新',
	'jp': '今すぐリリースして更新',
	'kr': '지금 새로고침을 해제하세요.',
}
const refresherRefreshingText = {
	'en-us': 'Refreshing...',
	'zh-cn': '正在刷新...',
	'jp': 'さわやか...',
	'kr': '상쾌...',
}
const refresherCompleteText = {
	'en-us': 'Refresh succeeded',
	'zh-cn': '刷新成功',
	'jp': '正常に更新',
	'kr': '새로고침 성공',
}

const loadingMoreDefaultText = {
	'en-us': 'Click to load more',
	'zh-cn': '点击加载更多',
	'jp': 'クリックしてさらにロード',
	'kr': '더 로드하려면 클릭하세요',
}
const loadingMoreLoadingText = {
	'en-us': 'Loading...',
	'zh-cn': '正在加载...',
	'jp': '読み込み中...',
	'kr': '로딩 중...',
}
const loadingMoreNoMoreText = {
	'en-us': 'No more data',
	'zh-cn': '没有更多了',
	'jp': 'もういや',
	'kr': '더 이상은 없어',
}
const loadingMoreFailText = {
	'en-us': 'Load failed,click to reload',
	'zh-cn': '加载失败，点击重新加载',
	'jp': 'ロードに失敗しました。クリックしてリロードしてください',
	'kr': '로드하지 못했습니다. 새로고침하려면 클릭하세요.',
}

const emptyViewText = {
	'en-us': 'No data',
	'zh-cn': '没有数据哦~',
	'jp': 'データなし',
	'kr': '데이터 없음',
}

const emptyViewReloadText = {
	'en-us': 'Reload',
	'zh-cn': '重新加载',
	'jp': 'リロード',
	'kr': '새로고침',
}

const emptyViewErrorText = {
	'en-us': 'Sorry,load failed',
	'zh-cn': '很抱歉，加载失败',
	'jp': '申し訳ありませんが、読み込みに失敗しました',
	'kr': '죄송합니다. 로드에 실패했습니다.',
}

const refresherUpdateTimeText = {
	'en-us': 'Last update: ',
	'zh-cn': '最后更新：',
	'jp': '最新のアップデート',
	'kr': '최근 업데이트',
}

const refresherUpdateTimeNoneText = {
	'en-us': 'None',
	'zh-cn': '无',
	'jp': 'それなし',
	'kr': '없이',
}

const refresherUpdateTimeTodayText = {
	'en-us': 'Today',
	'zh-cn': '今天',
	'jp': '今日',
	'kr': '오늘',
}

const refresherUpdateTimeYesterdayText = {
	'en-us': 'Yesterday',
	'zh-cn': '昨天',
	'jp': '昨日',
	'kr': '어제',
}

// 获取当前语言，格式为:zh-cn、zh-hant-cn、en-us。followSystemLanguage:获取的结果是否是在不跟随系统语言下获取到的
function getLanguage(followSystemLanguage = true) {
	return _getPrivateLanguage(false, followSystemLanguage);
}

// 获取当前语言，格式为:简体中文、繁體中文、English。followSystemLanguage:获取的结果是否是在不跟随系统语言下获取到的
function getLanguageName(followSystemLanguage = true) {
	const language = getLanguage(followSystemLanguage);
	const languageNameMap = {
		'zh-cn': '简体中文',
		'en-us': 'English',
		'jp':'日本',
		'kr':'대한민국'
	};
	return languageNameMap[language];
}

//设置当前语言，格式为:zh-cn、zh-hant-cn、en-us
function setLanguage(myLanguage) {
	uni.setStorageSync(i18nUpdateKey, myLanguage);
	uni.$emit(i18nUpdateKey, myLanguage);
}

// 插件内部使用，请勿直接调用
function _getPrivateLanguage(myLanguage, followSystemLanguage = true) {
	let systemLanguage = '';
	if (followSystemLanguage) {
		systemLanguage = uni.getSystemInfoSync().language;
	}
	let language = myLanguage || uni.getStorageSync(i18nUpdateKey) || systemLanguage;
	language = language.toLowerCase();
	var reg = new RegExp('_', '');
	language = language.replace(reg, '-');
	if (language.indexOf('zh') !== -1) {
		return 'zh-cn';
	}
	if (language.indexOf('en-us') !== -1) {
		return 'en-us';
	}
	if (language.indexOf('jp') !== -1) {
		return 'jp';
	}
	if (language.indexOf('kr') !== -1) {
		return 'kr';
	}
	return 'en-us';
}

module.exports = {
	refresherDefaultText,
	refresherPullingText,
	refresherRefreshingText,
	refresherCompleteText,
	refresherUpdateTimeText,
	refresherUpdateTimeNoneText,
	refresherUpdateTimeTodayText,
	refresherUpdateTimeYesterdayText,
	loadingMoreDefaultText,
	loadingMoreLoadingText,
	loadingMoreNoMoreText,
	loadingMoreFailText,
	emptyViewText,
	emptyViewReloadText,
	emptyViewErrorText,
	getLanguage,
	getLanguageName,
	setLanguage,
	_getPrivateLanguage,
}

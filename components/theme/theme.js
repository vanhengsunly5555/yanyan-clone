let pinkTheme = {
	theme: "blue",
	bg: "#1B78D2",
	color:'#1B78D2',
	shadow:'rgba(27, 120, 210, 0.3)',
	fu_color:'#ffffff',
	
	tabImg1:'/static/tabs/home@2x_blue.png',
	tabImg2:'/static/tabs/crypto@2x_blue.png',
	tabImg3:'/static/tabs/send@2x_blue.png',
	tabImg4:'/static/tabs/my@2x_blue.png',
	tabcolor:'#333333',
	tabselecColor:'#1B78D2',
	
}

let blueTheme = {
	theme: "pink",
	bg: "linear-gradient(-90deg, #FF97A9 0%, #FF668E 100%)",
	color:'#FF8AAB',
	shadow:'rgba(243, 161, 185, 0.3)',
	fu_color:'#3C2E33',
	
	tabImg1:'/static/tabs/home@2x.png',
	tabImg2:'/static/tabs/crypto@2x.png',
	tabImg3:'/static/tabs/send@2x.png',
	tabImg4:'/static/tabs/my@2x.png',
	tabcolor:'#3C2E33',
	tabselecColor:'#EA466F',
}

export {
	blueTheme,
	pinkTheme
}

import App from './App'
import messages from './static/locale/index'
//引入vuex
import store from './store'
// Introduce custom components 

// #ifndef VUE3
import Vue from 'vue'
import VueI18n from 'vue-i18n'
import { router } from './router/index.js'
import { req, apiUrl } from './api/index.js'  
import {toast} from './api/tos.js'
import uView from "uview-ui";
Vue.use(uView);

import myNav from "@/components/my-navbar/index.vue"
Vue.component('myNav', myNav)

import myNavcustom from "@/components/my-navbar-custom/index.vue"
Vue.component('myNavcustom', myNavcustom)


Vue.use(VueI18n)
const i18n = new VueI18n({
	// 默认语言
	locale: 'en-us',
	// 引入语言文件
	messages
})

//把vuex定义成全局组件
Vue.prototype.$store = store
Vue.config.productionTip = false
Vue.prototype.router = router
Vue.prototype.post = req
Vue.prototype.tos = toast
Vue.prototype.upload = apiUrl

App.mpType = 'app'
const app = new Vue({
  i18n,
  ...App,
  //挂载
  store
})
app.$mount()
// #endif

// #ifdef VUE3
import { createSSRApp } from 'vue'
import { createI18n } from 'vue-i18n'
const i18n = createI18n(i18nConfig)
export function createApp() {
  const app = createSSRApp(App)
  app.use(i18n)
  return {
    app
  }
}
// #endif
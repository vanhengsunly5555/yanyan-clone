import Vue from 'vue'
import Vuex from 'vuex'
import {blueTheme,pinkTheme} from '@/components/theme/theme.js'
Vue.use(Vuex)
const store = new Vuex.Store({
	state: {
		curr_name:'USDT',
		
		blueTheme: blueTheme,
		pinkTheme: pinkTheme,
		
		list: [1, 3, 5, 7, 9, 20, 30],
		counter:1
	},
	//实时监听state值的变化
	getters: {
		//页面调用this.$store.getters.filteredList => [7, 9, 20, 30]
		filteredList: state => {
		      return state.list.filter(item => item > 5)
		},
	},
	// 要调用mutations中的方法，必须通过commit的方式来提交
	
	mutations: {
		//页面调用this.$store.commit('increment',1)
		increment(state,num){
		    state.counter += num
		}
	},
	// action应该是mutation的前一步(异步操作出结果后再去调用mutation的方法) 
	actions: {
		//页面调用this.$store.dispatch('getADDCounter',1)
		getADDCounter(context,num){
		    context.commit('increment',num)
		}
	}
})
export default store

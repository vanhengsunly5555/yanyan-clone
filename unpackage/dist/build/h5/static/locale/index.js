import en from './en.json'
import zh from './zh.json'
import jp from './jp.json'
import kr from './kr.json'
export default {
	'en-us': en,
	'zh-cn': zh,
	'jp': jp,
	'kr': kr,
}
